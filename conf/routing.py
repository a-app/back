from django.urls import path

from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter

from a.characters.consumers import CharacterConsumer
from a.conversations.consumers import ConversationsConsumer, ConversationConsumer

application = ProtocolTypeRouter({
    'websocket': AuthMiddlewareStack(
        URLRouter([
            path('ws/characters/<uuid:uid>', CharacterConsumer),
            path('ws/conversations/', ConversationsConsumer),
            path('ws/conversations/<uuid:room_uuid>/', ConversationConsumer),
        ])
    ),
})
