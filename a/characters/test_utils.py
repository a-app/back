from functools import partial

from .utils import Encounter, Pender, introduce_characters, sleep


def append_encouter(encouters, player, encouter):
    encouters.append((encouter, player))


def test_single_pender():
    encouters, characters = [], ['foo', 'bar', 'baz']
    append = partial(append_encouter, encouters)
    first, second = Pender(introduce=append), Pender()
    assert id(first) == id(second)
    Pender().recover_characters(characters)
    sleep(10)
    assert Pender().characters == ['baz']
    encouter = Encounter('foo', None)
    encouter.join('bar')
    assert encouters == [(encouter, 'foo'), (encouter, 'bar')]


def test_match_single_player():
    encouters, characters = [], ['foo']
    append = partial(append_encouter, encouters)
    assert introduce_characters(characters, append, None) == ['foo']
    assert encouters == []


def test_match_two_characters():
    encouters, characters = [], ['foo', 'bar']
    append = partial(append_encouter, encouters)
    assert introduce_characters(characters, append, None) == []
    encouter = Encounter('foo', None)
    encouter.join('bar')
    assert encouters == [(encouter, 'foo'), (encouter, 'bar')]


def test_match_three_characters():
    encouters, characters = [], ['foo', 'bar', 'baz']
    append = partial(append_encouter, encouters)
    assert introduce_characters(characters, append, None) == ['baz']
    encouter = Encounter('foo', None)
    encouter.join('bar')
    assert encouters == [(encouter, 'foo'), (encouter, 'bar')]
