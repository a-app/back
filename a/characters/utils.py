from time import sleep
from threading import Thread

from a.conversations.models import Room
from .db import create_room
from .models import Character


class Encounter(object):

    def __init__(self, character, answer):
        self.character_answer = answer
        self.characters = [character]
        self.answers = {character: None}

    def __eq__(self, other):
        if isinstance(other, Encounter):
            return self.characters == other.characters
        return False

    def join(self, character):
        self.characters.append(character)
        self.answers[character] = None

    async def answer(self, character, accepted):
        self.answers[character] = accepted
        if None not in self.answers.values() and len(self.characters) == 2:
            foo, bar = self.characters[0], self.characters[1]
            if all(answer is True for answer in self.answers.values()):
                await create_room(foo.uid, bar.uid)
            await self.character_answer(foo, {
                'self': self.answers[foo],
                'other': self.answers[bar]
            })
            await self.character_answer(bar, {
                'self': self.answers[bar],
                'other': self.answers[foo]
            })

    async def encounterable(self, character):
        if len(self.characters) == 1:
            return await self.characters[0].encounterable(character)
        return False

    def introducable(self):
        return len(self.characters) == 2


def encounter_character(character, encounters):
    for encounter in encounters:
        if await encounter.encounterable(character):
            encounter.join(character)
            return True
    return False


def introduce_characters(characters, introduce, answer):
    encounters = []
    for character in characters:
        if not encounter_character(character, encounters):
            encounters.append(Encounter(character, answer))
    characters = []
    for encounter in encounters:
        if encounter.introducable():
            for character in encounter.characters:
                await introduce(character, encounter)
        else:
            characters.append(encounter.characters[0])
    return characters


class PenderIntroducer(type):

    _instance = None

    # TODO: clean introduce answer & encounterable functions, fix tests accordingly (passing instance of class with those functions insdead of play strs, force function implem with abstract base class / interface)
    def __call__(cls, introduce=None, answer=None, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super(
                PenderIntroducer, cls).__call__(*args, **kwargs)
            introducer = Introducer(introduce, answer)
            introducer.start()
        return cls._instance


class Pender(object, metaclass=PenderIntroducer):

    def __init__(self):
        self.characters = list()

    def new_character(self, character):
        self.characters.append(character)

    def recover_characters(self, characters):
        self.characters.extend(characters)

    def pending_characters(self):
        characters, self.characters = self.characters, []
        return characters


class Introducer(Thread):

    def __init__(self, introduce, answer):
        Thread.__init__(self, daemon=True)
        self.introduce = introduce
        self.answer = answer

    def run(self):
        while (True):
            sleep(5)
            characters = Pender().pending_characters()
            characters = introduce_characters(
                characters, self.introduce, self.answer)
            Pender().recover_characters(characters)
