from channels.generic.websocket import AsyncJsonWebsocketConsumer

from .models import Character
from .utils import Pender
from .db import encouterable_character


class CharacterConsumer(AsyncJsonWebsocketConsumer):

    async def connect(self):
        self.uid = self.scope['url_route']['kwargs']['uid']
        character = await Character.retrieve_instance({'uuid': self.uid})
        if character is None:
            await self.close()
        else:
            self.encounter = None
            groups = await Character.rooms_groups(character)
            for group in groups:
                await self.channel_layer.group_add(group, self.channel_name)
            await self.accept()
            # TODO: if no disconnet on navigation move call to cmd 'unread'
            await self.unread_conversations()

    async def disconnect(self, close_code):
        character = await Character.retrieve_instance({'uuid': self.uid})
        groups = await Character.rooms_groups(character)
        for group in groups:
            self.channel_layer.group_discard(group, self.channel_name)

    async def receive_json(self, content):
        cmd = content.get('cmd')
        if cmd is not None:
            if cmd == 'A' and self.encounter is None:
                await self.send_json({'wait': None})
                Pender(
                    introduce=CharacterConsumer.introduce,
                    answer=CharacterConsumer.answer,
                ).new_character(self)
            elif cmd in ('accept', 'refuse') and self.encounter is not None:
                encounter, self.encounter = self.encounter, None
                await encounter.answer(self, cmd == 'accept')

    async def encounterable(self, character):
        return await encouterable_character(self.uid, character.uid)

    async def introduce(self, encounter):
        self.encounter = encounter
        await self.send_json({'introduce': None})

    async def answer(self, answer):
        await self.send_json({'answer': answer})
        await self.unread_conversations()
        character = await Character.retrieve_instance({'uuid': self.uid})
        await Character.update_stats(
            character, answer['self'], answer['other'])

    async def send_message(self, event):
        await self.unread_conversations()

    async def unread_conversations(self):
        character = await Character.retrieve_instance({'uuid': self.uid})
        unread = await Character.unread_conversations(character)
        await self.send_json({'unread_conversations': unread})
