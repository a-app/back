from channels.db import database_sync_to_async

from .models import Character
from a.conversations.models import Room, Conversation


@database_sync_to_async
def encouterable_character(foo_uid, bar_uid):
    try:
        foo = Character.objects.get(uuid=foo_uid)
        bar = Character.objects.get(uuid=bar_uid)
        return foo != bar and foo not in bar.friends.all()
    except Character.DoesNotExist:
        return False


@database_sync_to_async
def create_room(foo_uid, bar_uid):
    try:
        foo = Character.objects.get(uuid=foo_uid)
        bar = Character.objects.get(uuid=bar_uid)
        # TODO: create rooms with better names
        room = Room.objects.create(label='room')
        Conversation.objects.create(room=room, character=foo)
        Conversation.objects.create(room=room, character=bar)
        foo.friends.add(bar)
    except Character.DoesNotExist:
        pass
