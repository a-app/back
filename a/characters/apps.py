from django.apps import AppConfig


class CharactersConfig(AppConfig):
    name = 'a.characters'
