from django.urls import path

from .views import create, detail

urlpatterns = [
    path('', create, name='create'),
    path('<uuid:uid>/', detail, name='detail'),
]
