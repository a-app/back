import uuid

from django.db import models
from django.contrib.auth import get_user_model
from channels.db import database_sync_to_async

from a.utils.db import AsyncMixin


class Character(AsyncMixin, models.Model):

    uuid = models.UUIDField(unique=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=32, null=False, blank=False)
    sentence = models.CharField(max_length=256, null=False, blank=False)
    own_accepted = models.PositiveIntegerField(
        null=False, blank=False, default=0)
    own_refused = models.PositiveIntegerField(
        null=False, blank=False, default=0)
    other_accepted = models.PositiveIntegerField(
        null=False, blank=False, default=0)
    other_refused = models.PositiveIntegerField(
        null=False, blank=False, default=0)
    user = models.ForeignKey(
        get_user_model(), related_name='characters', on_delete=models.CASCADE,
        null=True, blank=True, default=None)
    friends = models.ManyToManyField('self')

    def __str__(self):
        return self.name

    @database_sync_to_async
    def update_stats(self, own, other):
        if own:
            self.own_accepted += 1
        else:
            self.own_refused += 1
        if other:
            self.other_accepted += 1
        else:
            self.other_accepted += 1

    @database_sync_to_async
    def unread_conversations(self):
        return sum(
            conversation
            for conversation in self.conversations.all()
            if conversation.latest_message_is_unread()
        )

    @database_sync_to_async
    def rooms_groups(self):
        return [room.group() for room in self.rooms.all()]
