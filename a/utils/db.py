from channels.db import database_sync_to_async


class AsyncMixin(object):

    @database_sync_to_async
    @classmethod
    def create_instance(cls, data):
        return cls.objects.create(**data)

    @database_sync_to_async
    @classmethod
    def retrieve_instance(cls, data):
        try:
            return cls.objects.get(**data)
        except cls.DoesNotExist:
            return None

    @database_sync_to_async
    @classmethod
    def retrieve_instances(cls, data):
        return cls.objects.filter(**data)

    @database_sync_to_async
    @classmethod
    def apply_on_instance(cls, data, function, default):
        try:
            instance = cls.objects.get(**data)
            return function(instance)
        except cls.DoesNotExist:
            return default
