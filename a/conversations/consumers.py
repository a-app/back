from channels.generic.websocket import AsyncJsonWebsocketConsumer


class ConversationsConsumer(AsyncJsonWebsocketConsumer):

    async def connect(self):
        await self.accept()

    async def disconnect(self, close_code):
        pass

    async def receive_json(self, content):
        uid, cmd = content.get('uid'), content.get('cmd')
        if uid is not None:
            if cmd == 'create':
                pass
            elif cmd == 'list':
                pass


class ConversationConsumer(AsyncJsonWebsocketConsumer):

    async def connect(self):
        self.id = self.scope['url_route']['kwargs']['room_uuid']
        await self.accept()

    async def disconnect(self, close_code):
        pass

    async def receive_json(self, content):
        uid, cmd = content.get('uid'), content.get('cmd')
        if uid is not None:
            if cmd == 'create':
                pass
            elif cmd == 'list':
                pass
            elif cmd == 'A':
                pass
            elif cmd == 'B':
                pass
