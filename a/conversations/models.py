import uuid

from django.db import models
from django_extensions.db.models import TimeStampedModel

from a.characters.models import Character
from a.utils.db import AsyncMixin


class Room(AsyncMixin, TimeStampedModel):

    uuid = models.UUIDField(unique=True, default=uuid.uuid4, editable=False)
    label = models.CharField(max_length=128, null=False, blank=False)
    characters = models.ManyToManyField(
        Character, through='Conversation', related_name='rooms')

    def __str__(self):
        return self.label

    def group(self):
        return f'group-{self.uuid}'


class Message(AsyncMixin, TimeStampedModel):

    uuid = models.UUIDField(unique=True, default=uuid.uuid4, editable=False)
    content = models.TextField(null=False, blank=False)
    author = models.ForeignKey(
        Character, related_name='messages', on_delete=models.CASCADE,
        null=False, blank=False)
    conversation = models.ForeignKey(
        Room, related_name='messages', on_delete=models.CASCADE,
        null=False, blank=False)

    def __str__(self):
        return self.content


class Conversation(AsyncMixin, TimeStampedModel):

    last_read = models.ForeignKey(
        Message, related_name='conversations', on_delete=models.SET_NULL,
        null=True, blank=True, default=None)
    room = models.ForeignKey(
        Room, related_name='conversations', on_delete=models.CASCADE,
        null=False, blank=False)
    character = models.ForeignKey(
        Character, related_name='conversations', on_delete=models.CASCADE,
        null=False, blank=False)

    def __str__(self):
        return f'{self.character} @ {self.room}'

    def unread_count(self):
        if self.last_read is None:
            return self.room.messages.count()
        return self.room.messages.filter(
            created__gt=self.last_read.created).exclude(
                author__exact=self.character).count()

    def latest_message_is_unread(self):
        msgs = self.room.messages
        latest_message = None if msgs.count() == 0 else msgs.latest()
        return self.last_read is None or self.last_read != latest_message
