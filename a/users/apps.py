from django.apps import AppConfig


class UsersConfig(AppConfig):
    name = 'a.users'
